package pdf2text;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class Pdf2text {

    public static void main(String[] args) throws IOException {
        
        PDDocument doc = PDDocument.load(new File(args[0]));
        String text = new PDFTextStripper().getText(doc);
        
        String out = args[0].replace("pdf", "txt");
        FileWriter fw = new FileWriter(out);
        fw.write(text);
        fw.close();
    }
}
//https://www.tutorialkart.com/pdfbox/read-text-pdf-document-using-pdfbox-2-0/
//pdfbox-2.0.11.jar + fontbox-2.0.11.jar + commons-logging-1.2.jar
//http://pinter.org/archives/5782
